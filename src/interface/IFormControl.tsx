import { ValidationCreate } from "../validators/creatorValidation";
import { IconDefinition } from "@fortawesome/free-solid-svg-icons";

export interface IFormControl {
    valid: boolean,
    value: string,
    type: string,
    icon: IconDefinition,
    name: string,
    validations: ValidationCreate[] | null,
    validationErros?: string[],
    errorMessage: string,
    questionIcon: boolean,
}