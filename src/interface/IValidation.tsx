export interface IValidation {
    validate(inputValue: string): string
} 
