import { ValidationCreate } from "../creatorValidation"
import { IValidation } from "../../interface/IValidation"
import { WRONG_EMAIL } from "../../assets/translate/translate";

export class CreateValidationEmail extends ValidationCreate {
    createValidation(): IValidation {
        return new EmailValidation()
    }
}

export class EmailValidation implements IValidation {
    validate(email: string): string {
        const regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        return email.length !== 0 && !regexp.test(email) ? WRONG_EMAIL : ''
    }
}
