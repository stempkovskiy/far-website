import { ValidationCreate } from "../creatorValidation"
import { IValidation } from "../../interface/IValidation"
import { WRONG_LENGHT } from "../../assets/translate/translate"

export class CreatorMinLengthValidation extends ValidationCreate {

    constructor(private controlName: string, private length: number) {
        super()
    }
    createValidation(): IValidation {
        return new MinLength(this.controlName, this.length)
    }
}

export class MinLength implements IValidation {
    constructor(private controlName: string, private length: number) { }
    validate(value: string): string {
        return value.length !== 0 && value.length < 6 ? WRONG_LENGHT(this.controlName, value, this.length) : ''
    }
}