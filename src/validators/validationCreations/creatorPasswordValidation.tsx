import { ValidationCreate } from "../creatorValidation";
import { IValidation } from "../../interface/IValidation";
import { WRONG_PASSWORD_STRONG } from "../../assets/translate/translate";

export class CreatorPasswordValidation extends ValidationCreate {
    createValidation(): IValidation {
        return new PasswordValidation()
    }
}

export class PasswordValidation implements IValidation {
    validate(inputValue: string): string {
        var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])");
        return !strongRegex.test(inputValue) ? WRONG_PASSWORD_STRONG : ''
    }
}