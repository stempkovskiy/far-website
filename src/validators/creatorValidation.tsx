import { IValidation } from '../interface/IValidation'

export abstract class ValidationCreate {
    abstract createValidation(): IValidation
}

export const validation = (validationCreator: ValidationCreate, value: string) => {
    return validationCreator.createValidation().validate(value)
}