import { IconDefinition } from "@fortawesome/free-solid-svg-icons"
import { ValidationCreate } from "../validators/creatorValidation"
import { IFormControl } from "../interface/IFormControl"
import { WRONG_TEXT_DEFAULT } from "../assets/translate/translate"

export function createInput(_name: string, _icon: IconDefinition, _validations: ValidationCreate[] | null = null,
    _type: string = 'text', _questionIcon: boolean = false) {
    const upName = _name.charAt(0).toUpperCase() + _name.slice(1)
    const input: IFormControl = {
        errorMessage: WRONG_TEXT_DEFAULT(upName),
        type: _type,
        valid: false,
        icon: _icon,
        name: upName,
        validations: _validations,
        value: '',
        questionIcon: _questionIcon
    }
    return input
}

export function validation(_input: IFormControl): IFormControl {
    let errors: string[] = []
    if (_input.validations) {
        errors = validationErros(_input.validations, _input.value)
        if (errors.length !== 0) {
            _input.validationErros = errors
            _input.valid = false
        } else {
            _input.validationErros = []
            _input.valid = true
        }
        return _input
    }
    _input.valid = true
    return _input
}

function validationErros(validations: ValidationCreate[], value: string): string[] {
    const errors = validations.map(v => v.createValidation().validate(value))
    return errors.filter(error => error.trim())
}