import React, { useState } from 'react'
import './ForgotPassword.scss'
import { Input } from '../UI/Input/Input'
import { useDispatch } from 'react-redux'
import { Transition } from 'react-transition-group'
//  ICON 
import { faEnvelope, faEyeSlash, faKey } from '@fortawesome/free-solid-svg-icons'
//  INPUT
import { createInput, validation } from '../../framework/inputFramework'
import { IFormControl } from '../../interface/IFormControl'
//  VALIDATIONS INPUT
import { CreateValidationEmail } from "../../validators/validationCreations/creatorEmailValidation"
import { CreatorMinLengthValidation } from "../../validators/validationCreations/creatorMinLengthValidation"
import { CreatorPasswordValidation } from "../../validators/validationCreations/creatorPasswordValidation"
//  UI
import { Button } from '../UI/Button/Button'
//  ACTIONS
import { showNotification, hideNotification } from '../../redux/actions/notificationAction'
import { TypeNotification } from '../../redux/reducers/notificationReducer'
import { forgotPassword, resetPassword } from '../../http/POST/Identity/forgotPaswword'
import { ResetPasswordRequest } from '../../http/request/ResetPasswordRequest'
import { ForgotPasswordRequest } from '../../http/request/ForgotPasswordRequest'

const LENGTH = 6

export const ForgotPassword: React.FC = () => {
    const dispatch = useDispatch()
    const inputInitState: IFormControl[] = [
        createInput('code', faKey, null),
        createInput('new paswword', faEyeSlash,
            [
                new CreatorPasswordValidation(),
                new CreatorMinLengthValidation('New password', LENGTH)
            ], 'password', true)

    ]

    const emailInitInput: IFormControl = createInput('email', faEnvelope, [new CreateValidationEmail()])
    const btnInitState = {
        text: 'Reset paswword',
        disabled: true
    }

    const [inputState, setInputState] = useState<IFormControl[]>(inputInitState)
    const [emailInput, setEmailInput] = useState<IFormControl>(emailInitInput)

    const [btn, setBtn] = useState(btnInitState)
    const [toggle, setToggle] = useState<boolean>(false)

    const getValueFromEmailInput = (event: React.ChangeEvent) => {
        const text = event.target as HTMLInputElement
        const control = { ...emailInitInput }
        control.value = text.value

        const newInput = validation(control)
        if (newInput.valid && newInput.value.trim()) {
            setBtn(prev => ({ ...prev, disabled: false }))
        } else {
            setBtn(prev => ({ ...prev, disabled: true }))
        }
        setEmailInput(newInput)
    }

    const getValueFromInput = (event: React.ChangeEvent, name: string, inputId: number) => {
        const text = event.target as HTMLInputElement
        const controls = [...inputState]
        let input = controls.find(input => input.name === name)!
        input.value = text.value

        controls[inputId] = validation(input)

        const validInputs = inputState.filter(input => input.valid)
        console.log(validInputs)
        if (validInputs.length === inputState.length) {
            setBtn(prev => ({ ...prev, disabled: false }))
        } else {
            setBtn(prev => ({ ...prev, disabled: true }))
        }

        setInputState(controls)
    }

    const btnClick = async () => {
        if (btn.text !== 'Change password') {
            setBtn(prev => ({ ...prev, disabled: true }))
            const body: ForgotPasswordRequest = {
                email: emailInput.value
            }
            const response = await forgotPassword(body)
            if (response.success) {
                dispatch(showNotification(['secret code was send on your email'], TypeNotification.SUCCESS))
                setToggle(!toggle)
                setBtn(prev => ({ ...prev, text: 'Change password' }))
                setTimeout(() => {
                    dispatch(hideNotification())
                }, 6000)
            } else {
                dispatch(showNotification([response.errorMessage], TypeNotification.ERROR))
                setTimeout(() => {
                    dispatch(hideNotification())
                }, 6000)
                setBtn(prev => ({ ...prev, disabled: false }))
            }
        } else {
            setBtn(prev => ({ ...prev, disabled: true }))
            const _body: ResetPasswordRequest = {
                code: inputState[0].value,
                email: emailInput.value,
                newPassword: inputState[1].value,

            }
            const response = await resetPassword(_body)
            if (response.success) {
                dispatch(showNotification(['Your password was changed'], TypeNotification.SUCCESS))
                setTimeout(() => {
                    dispatch(hideNotification())
                }, 6000)
                setToggle(!toggle)
            } else {
                dispatch(showNotification([response.errorMessage], TypeNotification.ERROR))
                setBtn(prev => ({ ...prev, disabled: false }))
                setTimeout(() => {
                    dispatch(hideNotification())
                }, 6000)
            }
        }
    }

    return (
        <div style={{ width: '100%' }} className='mt-2'>
            <div className="row justify-content-center mb-4" >
                <div className='col-12 col-sm-7'>
                    <Input
                        type={emailInput.type}
                        value={emailInput.value}
                        icon={emailInput.icon}
                        placehoderValue={emailInput.name}
                        questionIcon={emailInput.questionIcon}
                        errorMessage={emailInput.errorMessage}
                        valid={emailInput.valid}
                        validationErros={emailInput.validationErros}
                        OnChangeValue={event => getValueFromEmailInput(event)}
                    />
                </div>
            </div>

            <Transition
                in={toggle}
                timeout={1000}
                unmountOnExit
                mountOnEnter
            >
                {
                    state =>
                        <div className={`row justify-content-around  wrap-change-password ${state}`}>
                            {
                                inputState.map((input, index) => {
                                    return (
                                        <div className='col-12 col-sm-7 mb-4' key={index}>
                                            <Input
                                                type={input.type}
                                                value={input.value}
                                                icon={input.icon}
                                                placehoderValue={input.name}
                                                questionIcon={input.questionIcon}
                                                errorMessage={input.errorMessage}
                                                valid={input.valid}
                                                validationErros={input.validationErros}
                                                OnChangeValue={event => getValueFromInput(event, input.name, index)} />
                                        </div>
                                    )
                                })
                            }
                        </div>

                }
            </Transition>


            <div className='row justify-content-center'>
                <div className='col-12 col-sm-7'>
                    <Button
                        text={btn.text}
                        disabled={btn.disabled}
                        OnClick={() => btnClick()}
                    />
                </div>
            </div>
        </div>
    )
}