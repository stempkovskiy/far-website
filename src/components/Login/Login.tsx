import React, { useState } from "react"
import './Login.scss'
import { useDispatch, useSelector } from "react-redux"
import { useHistory } from 'react-router-dom'
import { faEyeSlash, faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { Transition } from 'react-transition-group'
//  UI
import { Input } from "../UI/Input/Input"
import { Button } from "../UI/Button/Button"
import { Loader } from "../UI/Loader/Loader"
//  ACTIONS
import { showRegistration } from '../../redux/actions/signInAction'
import { showNotification, hideNotification } from '../../redux/actions/notificationAction'
//  VALIDATIONS INPUT
import { CreateValidationEmail } from "../../validators/validationCreations/creatorEmailValidation"
import { CreatorMinLengthValidation } from "../../validators/validationCreations/creatorMinLengthValidation"
import { CreatorPasswordValidation } from "../../validators/validationCreations/creatorPasswordValidation"
//  INPUT
import { IFormControl } from '../../interface/IFormControl'
import { validation, createInput } from "../../framework/inputFramework"
//  HTTP
import { SigIn, UserLoginRequest } from '../../http/POST/Identity/login'
import { AuthFailedResponse } from '../../http/response/AuthFailedResponse'
import { AppState } from "../../redux/store"
import { TypeNotification } from "../../redux/reducers/notificationReducer"
import { ForgotPassword } from "../ForgotPassword/ForgotPassword"

const LENGTH = 6


export const Login: React.FC = () => {
    const inputInitState: IFormControl[] = [
        createInput('email', faEnvelope, [new CreateValidationEmail()]),
        createInput('password', faEyeSlash, [
            new CreatorMinLengthValidation('Password', LENGTH),
            new CreatorPasswordValidation()
        ], 'password', true),
    ]
    const history = useHistory()
    const [inputState, setInputState] = useState<IFormControl[]>(inputInitState)
    const dispatch = useDispatch()
    const isShowReg = useSelector((store: AppState) => store.login.showRegistartion) as boolean
    const [enter, setEnter] = useState<boolean>(true)
    const [loading, setLoading] = useState<boolean>(false)
    const [forgottenPas, setforgottenPas] = useState<boolean>(false)


    const getValueFromInput = (event: React.ChangeEvent, name: string, inputId: number) => {
        const text = event.target as HTMLInputElement
        const controls = [...inputState]
        let input = controls.find(input => input.name === name)!
        input.value = text.value

        const newInput = validation(input)
        controls[inputId] = newInput

        const validInputs = inputState.filter(input => input.valid)
        if (validInputs.length === inputState.length) {
            setEnter(false)
        } else {
            setEnter(true)
        }

        setInputState(controls)
    }

    const renderInputs = () => {
        return inputState.map((input, index) => {
            return (
                <div className="col-11 col-sm-6 col-md-auto mt-4" key={index}>
                    <Input
                        placehoderValue={input.name}
                        icon={input.icon}
                        type={input.type}
                        valid={input.valid}
                        value={input.value}
                        errorMessage={input.errorMessage}
                        validationErros={input.validationErros}
                        questionIcon={input.questionIcon}
                        OnChangeValue={event => getValueFromInput(event, input.name, index)} />
                </div>
            )
        })
    }

    const signIn = () => {
        setLoading(true)
        setEnter(true)
        const body: UserLoginRequest = {
            Email: inputState[0].value,
            Password: inputState[1].value
        }
        SigIn.signInAsync(body).then(resp => {
            if (!(resp as AuthFailedResponse).errors) {
                setLoading(false)
                setEnter(false)
                history.push('homePage')
            } else {
                setLoading(false)
                setEnter(false)
                dispatch(showNotification((resp as AuthFailedResponse).errors, TypeNotification.ERROR))
                setTimeout(() => {
                    dispatch(hideNotification())
                }, 6000)
            }
        })
    }

    const forgottenPassword = () => {
        setforgottenPas(!forgottenPas)
    }

    return (
        <Transition
            in={!isShowReg}
            timeout={1000}
        >
            {state => <div className={`wrap-content ${state}`}>
                <div className="row justify-content-center">
                    <div className="col-auto">
                        <h1 className="text-white">Food&Reviews</h1>
                    </div>
                </div>
                <div className="row justify-content-center">
                    {renderInputs()}
                </div>
                <div className="row justify-content-center mt-4">
                    <div className="col-11 col-sm-7">
                        <Button text="Login"
                            OnClick={() => signIn()}
                            disabled={enter}
                        />
                    </div>
                </div>
                <div className="row justify-content-center mt-1">
                    <div className="col-auto">
                        <p className="link"
                            onClick={() => dispatch(showRegistration())}
                        >
                            Create New Account
                            </p>
                    </div>
                    <div className="col-auto">
                        <p className="link"
                            onClick={forgottenPassword}>
                            Forgotten  password?
                            </p>
                    </div>
                </div>

                {
                    forgottenPas ?
                        <div className='row justify-content-center'>
                            <div className='col-11' >
                                <ForgotPassword />
                            </div>
                        </div>
                        : null
                }

                {
                    loading ?
                        <div style={{ position: 'absolute', bottom: '-80px', left: '50%', marginLeft: '-40px' }}>
                            <Loader />
                        </div>
                        : null
                }

            </div>}
        </Transition>
    )
}