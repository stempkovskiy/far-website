import React, { useState, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import './Registration.scss'
import { Transition } from 'react-transition-group'
import { hideRegistration } from '../../redux/actions/signInAction'
import { faUser, faArrowUp, faEnvelope, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Input } from '../UI/Input/Input'
import { Button } from '../UI/Button/Button'
import { CreateValidationEmail } from '../../validators/validationCreations/creatorEmailValidation'
import { CreatorPasswordValidation } from '../../validators/validationCreations/creatorPasswordValidation'
import { CreatorMinLengthValidation } from '../../validators/validationCreations/creatorMinLengthValidation'
import { IFormControl } from '../../interface/IFormControl'
import { createInput, validation } from '../../framework/inputFramework'
import { RegisterIdentity } from '../../http/POST/Identity/registration'
import { UserRegistrationRequest } from '../../http/request/UserRegistrationRequest'
import { AuthSuccessResponse } from '../../http/response/AuthSuccessResponse'
import { UserInfo } from '../../http/userInfo'
import { AppState } from '../../redux/store'
import { Loader } from '../UI/Loader/Loader'
import { showNotification, hideNotification } from '../../redux/actions/notificationAction'
import { AuthFailedResponse } from '../../http/response/AuthFailedResponse'
import { TypeNotification } from '../../redux/reducers/notificationReducer'

const LENGTH_FOR_PASSWORD = 8
const LENGTH_FOR_NAME = 6

export const Registration: React.FC = () => {
    const inputInitState: IFormControl[] = [
        createInput('name', faUser, [new CreatorMinLengthValidation('Name', LENGTH_FOR_NAME)]),
        createInput('email', faEnvelope, [new CreateValidationEmail()]),
        createInput(
            'password',
            faEyeSlash, [
            new CreatorMinLengthValidation('Password', LENGTH_FOR_PASSWORD),
            new CreatorPasswordValidation()
        ], 'password', true),
        createInput('confirm password', faEyeSlash, null, 'password')
    ]


    const dispatch = useDispatch();
    const isShowReg = useSelector((store: AppState) => store.login.showRegistartion)
    const [inputState, setInputState] = useState<IFormControl[]>(inputInitState)
    const [disabledBtn, setDisabledBtn] = useState<boolean>(true)
    const [loading, setLoading] = useState<boolean>(false)
    const [classes, setClasses] = useState<string>('')
    const refInput = useRef<HTMLInputElement>(null)
    const refImage = useRef<HTMLImageElement>(null)

    const changeValue = (event: React.ChangeEvent, name: string, inputId: number) => {
        const text = event.target as HTMLInputElement
        const controls = [...inputState]
        let input = controls.find(input => input.name === name)!
        input.value = text.value
        const newInput = validation(input)

        if (newInput.name.toLowerCase() === 'confirm password') {
            newInput.valid = newInput.value === inputState[2].value
        }
        controls[inputId] = newInput

        const validInputs = inputState.filter(input => input.valid)
        if (validInputs.length === inputState.length) {
            setDisabledBtn(false)
        } else {
            setDisabledBtn(true)
        }
        setInputState(controls)
    }

    const register = async () => {
        setLoading(true)
        setDisabledBtn(true)
        const body: UserRegistrationRequest = {
            Email: inputState[1].value,
            Name: inputState[0].value,
            Password: inputState[2].value,
            Avatar: refImage.current?.src
        }
        const response = await RegisterIdentity.register(body)
        if ((response as AuthSuccessResponse).token) {
            setLoading(false)
            UserInfo.accessToken = (response as AuthSuccessResponse).token
            UserInfo.refreshToken = (response as AuthSuccessResponse).refreshToken
            dispatch(hideRegistration())
            dispatch(showNotification(['registration was successful'], TypeNotification.SUCCESS))
            setTimeout(() => {
                dispatch(hideNotification())
            }, 6000)
            setDisabledBtn(false)

        } else {
            setLoading(false)
            setDisabledBtn(false)
            dispatch(showNotification((response as AuthFailedResponse).errors, TypeNotification.ERROR))
            setTimeout(() => {
                dispatch(hideNotification())
            }, 6000)
        }
    }

    const loadAvatar = (event: React.ChangeEvent) => {
        var input = event.nativeEvent.target as HTMLInputElement
        var fileReader = new FileReader();
        if (input.files && input.files?.length != 0) {
            fileReader.readAsDataURL(input.files[0]);
            fileReader.onload = (event) => {
                setClasses('show')
                refImage.current!.src = event.target?.result as string
            }
        }
    }



    return (
        <Transition
            in={isShowReg}
            timeout={300}
            unmountOnExit
            mountOnEnter
        >
            {state => <div className={`wrap_reg_content ${state}`}>
                <div className='row justify-content-center'>
                    <div className='col-12 col-md-8'>
                        <div className='row justify-content-center'>
                            <div className='col-auto' style={{ position: 'relative' }}>
                                <div className='img_wrap'>
                                    <input type='file' ref={refInput} style={{ visibility: 'hidden' }} onChange={loadAvatar} />
                                    <div className="default-icon">
                                        <FontAwesomeIcon
                                            color='white'
                                            style={{ width: '30%', height: '30%' }}
                                            icon={faUser} />

                                        <div className={`avatar ${classes}`}>
                                            <img ref={refImage} style={{ width: '100%', height: '100%' }} />
                                        </div>
                                    </div>
                                </div>

                                <div className="load_img" onClick={() => { refInput.current?.click() }}>
                                    <FontAwesomeIcon
                                        color='white'
                                        style={{ width: '50%', height: '50%' }}
                                        icon={faArrowUp} />
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-around">
                            {inputState.map((input, index) => {
                                return (
                                    <div className="col-11 col-sm-5 mt-4" key={index}>
                                        <Input
                                            icon={input.icon}
                                            placehoderValue={input.name}
                                            type={input.type}
                                            value={input.value}
                                            valid={input.valid}
                                            errorMessage={input.errorMessage}
                                            validationErros={input.validationErros}
                                            questionIcon={input.questionIcon}
                                            OnChangeValue={event => changeValue(event, input.name, index)}
                                        />
                                    </div>
                                )
                            })}
                        </div>

                        <div className="row justify-content-around mt-4">
                            <div className="col-10 col-sm-5">
                                <Button
                                    text="Register"
                                    OnClick={() => register()}
                                    disabled={disabledBtn} />
                            </div>
                            <div className="w-100" />
                            <div className="col-auto">
                                <p className="text-white mt-1">Already have an account?
                            <span
                                        style={{ cursor: 'pointer', color: '#5663FF' }}
                                        onClick={() => dispatch(hideRegistration())}>Login</span>
                                </p>
                            </div>
                        </div>

                        {
                            loading ?
                                <div style={{ position: 'absolute', bottom: '-80px', left: '50%', marginLeft: '-40px' }}>
                                    <Loader />
                                </div>
                                : null
                        }
                    </div>
                </div>
            </div>
            }
        </Transition>
    )
}