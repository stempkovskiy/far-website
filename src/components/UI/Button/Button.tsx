import React from 'react'
import Radium from 'radium'
import './Button.scss'

interface IButton {
    text: string;
    height?: string;
    textcolor?: string;
    bgcolor?: string;
    borderRadius?: string;
    bgHoverColor?: string;
    boxShadow?: string;
    OnClick?(): void;
    disabled: boolean;
}
export const Button: React.FC<IButton> = Radium((props) => {
    const style = {
        color: props.textcolor,
        height: props.height,
        backgroundColor: props.bgcolor,
        borderRadius: props.borderRadius,
        boxShadow: props.boxShadow,
        ':hover': {
            backgroundColor: props.bgHoverColor
        }
    }

    return (
        <div className={`${props.disabled ? 'disabledBtn' : 'ui-button'} row justify-content-center`} style={style} onClick={props.OnClick}>
            <div className="col-auto align-self-center">
                <p>{props.text}</p>
            </div>
        </div>
    )
})