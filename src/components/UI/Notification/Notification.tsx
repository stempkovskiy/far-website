import React  from 'react'
import { useSelector } from 'react-redux'
import './Notification.scss'
import { Transition } from 'react-transition-group'
import { AppState } from '../../../redux/store'

export const Notification: React.FC = () => {
    const notification = useSelector((state: AppState) => state.notification)
    return (
        <Transition
            in={notification.isShow}
            timeout={1000}
            unmountOnExit
            mountOnEnter
        >
            {
                state =>
                    <div className={`row justify-content-center m-2 notification ${state}`}>
                        <div className="col-auto">
                            <div className={`wrap-notification ${notification.type}`}>
                                {
                                    notification.errors.map((error, index) =>
                                        <div key={index}>
                                            <small className="text-white">{error}</small>
                                            <br />
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                    </div>
            }
        </Transition>

    )
}