import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import "./Input.scss";
import { IconDefinition, faEye, faEyeSlash, faQuestionCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { hidePassword, showPassword } from '../../../redux/actions/signInAction'
import { Transition } from 'react-transition-group'


interface IInput {
  placehoderValue: string,
  value: string,
  icon: IconDefinition,
  type: string,
  valid?: boolean,
  color?: string,
  errorMessage?: string,
  validationErros?: string[],
  questionIcon: boolean
  OnChangeValue?(event: React.ChangeEvent<HTMLInputElement>): void
}

export const Input: React.FC<IInput> = (props) => {
  const [fontIcon, setFontIcon] = useState<IconDefinition>(props.icon)
  const [textType, setTextType] = useState<string>(props.type)
  const [wrongText, setWrongText] = useState<boolean>(false)
  const dispatch = useDispatch()
  const passwordState = useSelector<any>(state => state.login.showPassword)
  let classes: string[] = ['wrap_input']

  if (props.value.trim()) {
    if (props.valid) {
      classes.push('success')
    } else classes.push('invalid')
  }


  const openEye = () => {
    if (!passwordState) {
      dispatch(showPassword())
      setFontIcon(faEye)
      setTextType('text')
    } else {
      dispatch(hidePassword())
      setFontIcon(faEyeSlash)
      setTextType('password')
    }
  }

  const element = props.type === 'password' ?
    <FontAwesomeIcon icon={fontIcon} color="white" style={{width:'20px', height:'20px'}} onClick={openEye} />
    : <FontAwesomeIcon icon={props.icon} color="white" style={{width:'20px', height:'20px'}}/>


  const errors =
    <Transition
      in={!props.valid && props.value !== ''}
      timeout={500}
      mountOnEnter
      unmountOnExit
    >
      {
        state =>
          <div className={`error_text ${state}`}>
            <div className="wrap_wrong_text">
              <small className="mr-2 text-truncate">{props.errorMessage}</small>
              {
                props.questionIcon ? <FontAwesomeIcon
                  icon={faQuestionCircle}
                  color="#db002c"
                  className="question"
                  onMouseEnter={() => setWrongText(true)}
                  onMouseLeave={() => setWrongText(false)}
                />
                  : null
              }

            </div>
            <Transition
              in={wrongText}
              timeout={500}
              mountOnEnter
              unmountOnExit
            >
              {
                state =>
                  <div className={`wrap_wrong_info ${state}`}>
                    {
                      props.validationErros?.map((error, index) => {
                        return (
                          <div key={index} >
                            <small >{error}</small>
                          </div>
                        )
                      })
                    }
                  </div>
              }
            </Transition>
          </div>
      }
    </Transition>

  return (
    <>
      <div className={classes.join(' ')}>
        {element}
        <input type={textType}
          placeholder={props.placehoderValue}
          className="input ml-3"
          value={props.value}
          onChange={props.OnChangeValue} />
      </div>
      {errors}
    </>
  );
};
