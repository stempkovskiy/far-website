import React, { useState, useEffect } from "react";
import "./App.scss";
import { SignIn } from "./pages/SignIn/SignIn";
import { Switch, Route, Redirect } from "react-router-dom"
import { ForgotPassword } from "./components/ForgotPassword/ForgotPassword"
import { Error404 } from "./pages/Error/Error"
import { Home } from "./pages/Home/Home";



const App: React.FC = () => {

  const [token, setToken] = useState<string>('')
  useEffect(() => {
    const accessToken = window.localStorage.getItem('accessToken') || ''
    setToken(accessToken)
  }, [])

  return (
    <>
      <Switch>
        <Route path="/" exact component={SignIn} />
        {
          token.trim() ?
            <Route path="/forgotPassword" component={ForgotPassword} /> : <Redirect from="/forgotPassword" to="/" />
        }
        {
          token.trim() ?
            <Route path="/homePage" component={Home} /> : <Redirect from="/homePage" to="/" />
        }
        <Route component={Error404} />
      </Switch>
    </>
  )
};

export default App;
