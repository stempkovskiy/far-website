import React from 'react'
import errorImg from '../../assets/img/backgrounds/404.gif'
import { Button } from '../../components/UI/Button/Button'
import { useHistory } from 'react-router-dom'
export const Error404: React.FC = () => {
    const history = useHistory()
    return (
        <>
            <div className="row justify-content-center">
                <div className="col-auto">
                    <img src={errorImg} alt='Eror 404' />
                </div>
            </div>

            <div className="row justify-content-center">
                <div className="col-11 col-md-2">
                    <Button disabled={false} OnClick={() => history.push('/')} text="Go home"/>
                </div>
            </div>

        </>
    )
}