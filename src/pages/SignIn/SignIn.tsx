import React from "react"
import "./SignIn.scss";
import { Registration } from "../../components/Registration/Registration"
import { Login } from "../../components/Login/Login"
import { Notification } from '../../components/UI/Notification/Notification'


export const SignIn: React.FC = () => {

  return (
    <div className="wrap">
      <div className="dark-font">
        <Notification />
        <Login />
        <Registration />
      </div>
    </div>
  );
};
