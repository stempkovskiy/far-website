import React from "react"
import bgImg from '../../assets/img/backgrounds/bg_2.jpg'


export const Home = () => {

    return (
        <>
            <img src={bgImg} style={{ width: '100vw', height: '100vh' }} />
        </>
    )
}