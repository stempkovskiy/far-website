import { SHOW_PASSWORD, HIDE_PASSWORD, GET_VALUE_FROM_INPUT, SHOW_REGISTRATION, HIDE_REGISTRATION } from '../types'
import { IActions } from '../../interface/Ireducer'

export const showPassword = () => {
    const action: IActions = {
        type: SHOW_PASSWORD,
        payload: true
    }
    return action
}

export const hidePassword = () => {
    const action: IActions = {
        type: HIDE_PASSWORD,
        payload: false
    }
    return action
}

export const getValueFromInput = (type: string, value: string) => {
    const action: IActions = {
        type: GET_VALUE_FROM_INPUT,
        payload: {
            user: {
                type,
                value
            }
        }
    }
    return action
}

export const showRegistration = () => {
    const action: IActions = {
        type: SHOW_REGISTRATION,
        payload: true
    }
    return action
}

export const hideRegistration = () => {
    const action: IActions = {
        type: HIDE_REGISTRATION,
        payload: false
    }
    return action
}

