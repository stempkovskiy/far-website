import { HIDE_NOTIFICATION, SHOW_NOTIFICATION } from '../types'
import { IActions } from '../../interface/Ireducer'
import { TypeNotification } from '../reducers/notificationReducer'

export const showNotification = (errors: string[], type: TypeNotification) => {
    const action: IActions = {
        type: SHOW_NOTIFICATION,
        payload: {
            errors,
            isShow: true,
            type
        }
    }
    return action
}

export const hideNotification = () => {
    const action: IActions = {
        type: HIDE_NOTIFICATION,
        payload: {
            isShow: false
        }
    }
    return action
}