import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from 'redux-thunk'
import { signInReducer } from "./reducers/signInReducer"
import { notificationReducer } from './reducers/notificationReducer'

const rootReducer = combineReducers({
  login: signInReducer,
  notification: notificationReducer
});

export type AppState = ReturnType<typeof rootReducer> 

export default createStore(rootReducer, applyMiddleware(thunk));
