import { IActions } from '../../interface/Ireducer'
import { SHOW_NOTIFICATION, HIDE_NOTIFICATION } from '../types'

export enum TypeNotification {
    SUCCESS = 'success',
    ERROR = 'error',
    WRONG = 'wrong',
}

interface INotificationState {
    errors: string[],
    isShow: boolean,
    type: TypeNotification
}

const initialstate: INotificationState = {
    errors: [],
    isShow: false,
    type: TypeNotification.SUCCESS
}

export const notificationReducer = (state = initialstate, action: IActions): INotificationState => {
    switch (action.type) {
        case SHOW_NOTIFICATION: return { ...state, ...action.payload }
        case HIDE_NOTIFICATION: return { ...state, ...action.payload }
        default: return state
    }
}
