import { IActions } from '../../interface/Ireducer'
import { HIDE_PASSWORD, SHOW_PASSWORD, GET_VALUE_FROM_INPUT, HIDE_REGISTRATION, SHOW_REGISTRATION } from '../types'

const initialstate = {
    showPassword: false,
    showRegistartion: false,
    password: '',
    login: ''

}

export const signInReducer = (state = initialstate, action: IActions) => {
    switch (action.type) {
        case SHOW_PASSWORD: return { ...state, showPassword: action.payload }
        case HIDE_PASSWORD: return { ...state, showPassword: action.payload }
        case GET_VALUE_FROM_INPUT: {
            if (action.payload!.user.type === 'text') {
                return { ...state, login: action.payload!.user.value }
            } else {
                return { ...state, password: action.payload!.user.value }
            }

        }
        case SHOW_REGISTRATION: return {...state, showRegistartion: action.payload}
        case HIDE_REGISTRATION: return {...state, showRegistartion: action.payload}
        default: return state
    }
}
