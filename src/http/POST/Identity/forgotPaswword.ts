import { LOCAL_URL } from '../../urlPath'
import axios, { AxiosError } from 'axios'
import { ForgotPasswordResponse } from '../../response/ForgotPasswordResponse'
import { ResetPasswordRequest } from '../../request/ResetPasswordRequest'
import { ForgotPasswordRequest } from '../../request/ForgotPasswordRequest'

export const forgotPassword = async (request: ForgotPasswordRequest): Promise<ForgotPasswordResponse> => {
    try {

        var response = await axios.post<ForgotPasswordResponse>(LOCAL_URL + 'api/v1/identity/forgotPassword', request)
        return response.data
    } catch (error) {
        return (error as AxiosError<ForgotPasswordResponse>).response!.data
    }
} 

export const resetPassword = async (_body: ResetPasswordRequest): Promise<ForgotPasswordResponse> => {
    try {
        const response = await axios.post<ForgotPasswordResponse>(LOCAL_URL + 'api/v1/identity/resetPassword', _body)
        return response.data
    } catch (error) {
        return (error as AxiosError<ForgotPasswordResponse>).response!.data
    }
}