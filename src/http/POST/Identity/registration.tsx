import axios, { AxiosError } from 'axios'
import { LOCAL_URL } from '../../urlPath'
import { AuthSuccessResponse } from '../../response/AuthSuccessResponse'
import { UserRegistrationRequest } from '../../request/UserRegistrationRequest'
import { AuthFailedResponse } from '../../response/AuthFailedResponse'

export class RegisterIdentity {
    static async register(_body: UserRegistrationRequest): Promise<AuthFailedResponse | AuthSuccessResponse> {
        try {
            var response = await axios.post<AuthSuccessResponse>(LOCAL_URL + 'api/v1/identity/register', _body)
            return response.data
        } catch (error) {
            const fail: AuthFailedResponse = {
                errors: (error as AxiosError<AuthFailedResponse>).response?.data.errors!
            }
            return fail
        }
    }

}