import axios, { AxiosError } from 'axios'
import { LOCAL_URL } from '../../urlPath'
import { UserInfo } from '../../userInfo'
import { AuthSuccessResponse } from '../../response/AuthSuccessResponse'
import { AuthFailedResponse } from '../../response/AuthFailedResponse'

export interface UserLoginRequest {
    Email: string
    Password: string
}

export class SigIn {
    static async signInAsync(_body: UserLoginRequest){
        try {
            const response = await axios.post<AuthSuccessResponse>(LOCAL_URL + 'api/v1/identity/login', _body)
            UserInfo.accessToken = response.data.token
            UserInfo.refreshToken = response.data .refreshToken
            UserInfo.setHeader()
            UserInfo.setLocalStorage()
            return response.data

        } catch (error) {
            const fail: AuthFailedResponse = {
                errors: (error as AxiosError<AuthFailedResponse>).response?.data.errors!
            }
            return fail
        }
    }
}