export interface ResetPasswordRequest {
    code: string,
    newPassword: string,
    email: string
}
