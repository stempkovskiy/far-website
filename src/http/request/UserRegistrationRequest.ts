export interface UserRegistrationRequest {
    Name: string,
    Email: string,
    Password: string,
    Avatar?: string
}
