import axios from 'axios'
import { LOCAL_URL } from '../urlPath'

export type UserInfoResponse = {
    name: string
    email: string
    avatar?: string
    success: boolean
}

export class UserInfo {
    static async getUserInfoAsync(_email: string) {
        const resp = await axios.get<UserInfoResponse>(LOCAL_URL + `api/v1/user/getInfo?email=${_email}`)
        return resp.data
    }
}
