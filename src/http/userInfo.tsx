export class UserInfo {

    static accessToken: string
    static refreshToken: string
    static header: string

    static setHeader() {
        this.header = `Bearer ${this.accessToken}`
    }

    static setLocalStorage() {
        window.localStorage.setItem('accessToken', UserInfo.accessToken)
        window.localStorage.setItem('refreshToken', UserInfo.refreshToken)
    }
}