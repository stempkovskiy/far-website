export interface ForgotPasswordResponse {
    success: boolean,
    errorMessage: string
}