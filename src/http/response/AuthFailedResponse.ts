export interface AuthFailedResponse {
    errors: string[]
}