export const WRONG_TEXT_DEFAULT = (field: string) => `Incorrectly entered field "${field}"`
export const WRONG_EMAIL = 'Incorrectly entered email address'
export const WRONG_PASSWORD = 'Incorrectly entered password'
export const WRONG_PASSWORD_STRONG = 
   `The string must contain at least 1 lowercase alphabetical character.
   The string must contain at least 1 uppercase alphabetical character.
   The string must contain at least 1 numeric character.
   The string must contain at least one special character [!@#$%^&*].` 
export const WRONG_LENGHT = (controlName: string, value: string, length: number) =>
    `The number of characters in the ${controlName} field must be more than ${value.length}/${length}.`

